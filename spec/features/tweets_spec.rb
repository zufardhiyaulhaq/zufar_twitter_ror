require 'rails_helper'

feature 'Tweets', type: :feature do
  describe '#create' do
    context 'create new tweets' do
      it 'should be success' do
        visit root_path
        within('form') do
          fill_in 'tweets_content', with: 'hahaha'
        end
        click_button 'Tweets!'
        expect(page.status_code).to be(200)
      end
    end
  end

  describe '#index' do
    context 'create new tweets' do
      it 'should apper' do
        Tweet.create!(content: 'testing tweets')
        visit root_path
        expect(page).to have_text 'testing tweets'
      end
    end
  end

  describe '#delete' do
    context 'delete tweets' do
      it 'should be success' do
        tweet = Tweet.create!(content: 'testing tweets')
        visit root_path
        within "#tweet_#{tweet.id}" do
          click_link 'delete'
        end
        expect(page.status_code).to be(200)
      end
    end
  end
end
