## Infrastructure Setup

![Infrastructure](pictures/week1-vagrant-infra.png)

Requirement:
- [Vagrant](https://www.vagrantup.com/docs/installation/)
- [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
- [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

### Installation
- Create infrastructure environment virtual machine with Vagrant.
```
vagrant up
```

- Check infrastructure environment
```
vagrant status
```

This will create the following virtual machine:

| Name | IP Address | Description | Username | password |
|-|-|-|-|-|
| db | 192.168.50.10 | Database VM | vagrant | vagrant |
| web-1 | 192.168.50.21 | Web service VM | vagrant | vagrant |
| web-2 | 192.168.50.22 | Web service VM | vagrant | vagrant |
| web-3 | 192.168.50.23 | Web service VM | vagrant | vagrant |
| gitlab-runner | 192.168.50.30 | Runner VM | vagrant | vagrant |
| lb | 192.168.50.100 | Load Balancer VM  | vagrant | vagrant |

- Generate ssh keypair in host operating system if necessary
```
mac$ ssh-keygen
```

- Copy host operating system public key to each virtual machine
```
mac$ ssh-copy-id vagrant@192.168.50.10
mac$ ssh-copy-id vagrant@192.168.50.21
mac$ ssh-copy-id vagrant@192.168.50.22
mac$ ssh-copy-id vagrant@192.168.50.23
mac$ ssh-copy-id vagrant@192.168.50.30
mac$ ssh-copy-id vagrant@192.168.50.100
```
make sure each user in all VM have sudoers access

- Change configuration in `provisioning/group_vars/all.yml` if neccessary

| Config | Description |
|-|-|
| database.type | only support postgresql |
| postgresql.version | version of postgresql |
| postgresql.host | postgresql IP Address |
| postgresql.database | postgresql Database name |
| postgresql.user | postgresql Database user |
| postgresql.password | postgresql Database password |
| postgresql.allowed_hosts | network allowed to access database |
| ruby.version | ruby version |
| ruby.download_url | ruby download file, get url from [official source](https://www.ruby-lang.org/en/downloads/) |
| bundler.version | bundler version |
| app.user | user craeted for running rails |
| app.directory | directory created to store the rails app |
| app.name | rails app name |
| app.port | rails app port exposed |
| app.master_key | master_key for the rails app |
| runner.url | URL for runner |
| runner.registration_token | token for runner |
| runner.shell.description | description for shell runner |
| runner.shell.tag | tag for shell runner |
| runner.docker.description | description for docker runner |
| runner.docker.tag | tag for docker runner |
| runner.docker.image | default image for docker runner |

Basically you only need to change `runner.registration_token` with registration token from `gitlab repository`.

- Change inventory in `provisioning/hosts/hosts` if neccessary
- Provisioning & preparing web, database, load balancer, and gitlab-runner machine
```
ansible-playbook -i provisioning/hosts/hosts provisioning/installation.yml
```

### Deployment CI & CD
- Generate ssh keypair in gitlab-runner node user `gitlab-runner` if necessary
```
ssh vagrant@192.168.50.30
```

```
vagrant$ sudo su
root# su gilab-runner
gitlab-runner$ ssh-keygen
```

- Copy gitlab-runner node public key to each web virtual machine
```
gitlab-runner$ ssh-copy-id vagrant@192.168.50.21
gitlab-runner$ ssh-copy-id vagrant@192.168.50.22
gitlab-runner$ ssh-copy-id vagrant@192.168.50.23
```

- change apps code and push to repository, the CI/CD will automatically run.
