# Pipeline
This project use trunk based development. That mean every developer should push to `master`. When application ready to release, `authorized person` can create a `tag release` to release to `production`.

#### Limitation
Because of the limitations of the infrastructure. This project doesn't have staging environment.

## Workflow
#### Developer
Developer only need to push to master branch and gitlab will execute a test to make sure the application can running.

![Developer Workflow](pictures/week1-push-master.png)

## Release
`Authorized person` create a `tag release`. Gitlab will execute `test`, `build`, and `deploy` stage to deploy to production machine.

![Release Workflow](pictures/week1-tag-release.png)

The release tag should follow [semantic versioning](https://semver.org/) without label.

example:
```
0.0.1
0.1.1
1.0.0
```
