## Description
A Simple web like twitter that can create tweet, delete and see the tweets.

## Environment Setup
- Ruby 2.6.5
- Bundler 2.1.4
- Rails 6.0.2.1

Install all dependency using bundle in root of directory project.
```bash
bundle install
```

## Run Test
using rspec & rubocop
```bash
bundle exec rake
```

to open the coverage result in browser
```bash
bundle exec rake coverage
```

## Run Instruction
set database environment to match your environment application
- Production environment

| Config | Description |
|-|-|
| RAILS_ENV | production |
| PROD_DATABASE_ADAPTER | production database adapter |
| PROD_DATABASE_NAME | production database name |
| PROD_DATABASE_USERNAME | production database username |
| PROD_DATABASE_PASSWORD | production database password |
| PROD_DATABASE_HOST | production database ip address |
| SECRET_KEY_BASE | production environment need to set the key, fill with long string |

- Test environment

| Config | Description |
|-|-|
| RAILS_ENV | test |
| TEST_DATABASE_ADAPTER | test database adapter |
| TEST_DATABASE_NAME | test database name |
| TEST_DATABASE_USERNAME | test database username |
| TEST_DATABASE_PASSWORD | test database password |
| TEST_DATABASE_HOST | test database ip address |

- Development environment

| Config | Description |
|-|-|
| RAILS_ENV | development |
| DEV_DATABASE_ADAPTER | development database adapter |
| DEV_DATABASE_NAME | development database name |
| DEV_DATABASE_USERNAME | development database username |
| DEV_DATABASE_PASSWORD | development database password |
| DEV_DATABASE_HOST | development database ip address |

migrate the database
```bash
bundle exec rails db:migrate
```

to run the application
```bash
bundle exec rails server
```

open browser and open the app
```bash
http://127.0.0.1:3000
```

## CI & CD
This project enable CI & CD to make deployment more fast. To setup the infrastructure, go to [infrastructure setup docs](docs/INFRASTRUCTURE.md).

This project use trunk based development. To see how its work, go to [pipeline workflow docs](docs/PIPELINE.md)
